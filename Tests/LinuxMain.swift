import XCTest

import novationlaunchTests

var tests = [XCTestCaseEntry]()
tests += novationlaunchTests.allTests()
XCTMain(tests)