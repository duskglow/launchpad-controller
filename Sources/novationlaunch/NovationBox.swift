//
//  NovationBox.swift
//  novationcontroller
//
//  Created by Russell Miller on 7/28/18.
//    novationlaunch - an interface to Novation Launchpad controllers
//  Copyright (C) 2018  Russell Miller

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa

class NovationBox: NSBox {

    @objc dynamic var X: u_char = 0
    @objc dynamic var Y: u_char = 0
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
