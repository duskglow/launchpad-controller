//
//  ViewController.swift
//  novationcontroller
//
//  Created by Russell Miller on 7/28/18.
//    novationlaunch - an interface to Novation Launchpad controllers
//  Copyright (C) 2018  Russell Miller

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa
import CoreMIDI

class ViewController: NSViewController {

    var SessionMIDIData = [[0]]
    var BoxLookup: [NovationBox?]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        

        self.BoxLookup = [  nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, I1, I2, I3, I4, I5,
                            I6, I7, I8, I9, nil, H1, H2, H3, H4, H5, H6, H7, H8, H9, nil, G1,
                            G2, G3, G4, G5, G6, G7, G8, G9, nil, F1, F2, F3, F4, F5, F6, F7,
                            F8, F9, nil, E1, E2, E3, E4, E5, E6, E7, E8, E9, nil, D1, D2, D3,
                            D4, D5, D6, D7, D8, D9, nil, C1, C2, C3, C4, C5, C6, C7, C8, C9,
                            nil, B1, B2, B3, B4, B5, B6, B7, B8, B9 ]
        MIDI.addObserver(self)
    }
    
    override func viewDidDisappear() {
        super.viewDidDisappear()
        
        MIDI.removeObserver(self)
    }

    @IBOutlet weak var A1: NovationBox!
    @IBOutlet weak var A2: NovationBox!
    @IBOutlet weak var A3: NovationBox!
    @IBOutlet weak var A4: NovationBox!
    @IBOutlet weak var A5: NovationBox!
    @IBOutlet weak var A6: NovationBox!
    @IBOutlet weak var A7: NovationBox!
    @IBOutlet weak var A8: NovationBox!
    @IBOutlet weak var B1: NovationBox!
    @IBOutlet weak var B2: NovationBox!
    @IBOutlet weak var B3: NovationBox!
    @IBOutlet weak var B4: NovationBox!
    @IBOutlet weak var B5: NovationBox!
    @IBOutlet weak var B6: NovationBox!
    @IBOutlet weak var B7: NovationBox!
    @IBOutlet weak var B8: NovationBox!
    @IBOutlet weak var B9: NovationBox!
    @IBOutlet weak var C1: NovationBox!
    @IBOutlet weak var C2: NovationBox!
    @IBOutlet weak var C3: NovationBox!
    @IBOutlet weak var C4: NovationBox!
    @IBOutlet weak var C5: NovationBox!
    @IBOutlet weak var C6: NovationBox!
    @IBOutlet weak var C7: NovationBox!
    @IBOutlet weak var C8: NovationBox!
    @IBOutlet weak var C9: NovationBox!
    @IBOutlet weak var D1: NovationBox!
    @IBOutlet weak var D2: NovationBox!
    @IBOutlet weak var D3: NovationBox!
    @IBOutlet weak var D4: NovationBox!
    @IBOutlet weak var D5: NovationBox!
    @IBOutlet weak var D6: NovationBox!
    @IBOutlet weak var D7: NovationBox!
    @IBOutlet weak var D8: NovationBox!
    @IBOutlet weak var D9: NovationBox!
    @IBOutlet weak var E1: NovationBox!
    @IBOutlet weak var E2: NovationBox!
    @IBOutlet weak var E3: NovationBox!
    @IBOutlet weak var E4: NovationBox!
    @IBOutlet weak var E5: NovationBox!
    @IBOutlet weak var E6: NovationBox!
    @IBOutlet weak var E7: NovationBox!
    @IBOutlet weak var E8: NovationBox!
    @IBOutlet weak var E9: NovationBox!
    @IBOutlet weak var F1: NovationBox!
    @IBOutlet weak var F2: NovationBox!
    @IBOutlet weak var F3: NovationBox!
    @IBOutlet weak var F4: NovationBox!
    @IBOutlet weak var F5: NovationBox!
    @IBOutlet weak var F6: NovationBox!
    @IBOutlet weak var F7: NovationBox!
    @IBOutlet weak var F8: NovationBox!
    @IBOutlet weak var F9: NovationBox!
    @IBOutlet weak var G1: NovationBox!
    @IBOutlet weak var G2: NovationBox!
    @IBOutlet weak var G3: NovationBox!
    @IBOutlet weak var G4: NovationBox!
    @IBOutlet weak var G5: NovationBox!
    @IBOutlet weak var G6: NovationBox!
    @IBOutlet weak var G7: NovationBox!
    @IBOutlet weak var G8: NovationBox!
    @IBOutlet weak var G9: NovationBox!
    @IBOutlet weak var H1: NovationBox!
    @IBOutlet weak var H2: NovationBox!
    @IBOutlet weak var H3: NovationBox!
    @IBOutlet weak var H4: NovationBox!
    @IBOutlet weak var H5: NovationBox!
    @IBOutlet weak var H6: NovationBox!
    @IBOutlet weak var H7: NovationBox!
    @IBOutlet weak var H8: NovationBox!
    @IBOutlet weak var H9: NovationBox!
    @IBOutlet weak var I1: NovationBox!
    @IBOutlet weak var I2: NovationBox!
    @IBOutlet weak var I3: NovationBox!
    @IBOutlet weak var I4: NovationBox!
    @IBOutlet weak var I5: NovationBox!
    @IBOutlet weak var I6: NovationBox!
    @IBOutlet weak var I7: NovationBox!
    @IBOutlet weak var I8: NovationBox!
    @IBOutlet weak var I9: NovationBox!
    
    required init?(coder: NSCoder) {
        self.SessionMIDIData = [[0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x00],
                                [0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59],
                                [0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F],
                                [0x3D, 0x3E, 0x3F, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45],
                                [0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B],
                                [0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31],
                                [0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27],
                                [0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D],
                                [0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13]]
        self.BoxLookup = [nil]
        super.init(coder: coder)
    }
}

extension ViewController: MIDIObserver {
    
    func receive(_ notice: MIDINotice) {
        print(notice)
    }
    
    func receive(_ packet: MIDIPacket, from source: MIDISource) {
        switch packet.message {
        case .noteOn:
            print(self.BoxLookup)
            let box = self.BoxLookup[Int(packet.data1)]
            if box == nil {
                print("Empty box, ignoring")
            } else {
                if packet.data2 == 127 {
                    DispatchQueue.main.async {
                        box?.borderColor = NSColor.systemRed
                    }
                }
                else if packet.data2 == 0 {
                    DispatchQueue.main.async {
                        box?.borderColor = NSColor.black
                    }
                }
            }
            
            print(packet.message, source)
        case .noteOff, .controlChange, .pitchBendChange:
            print(packet.message, source)
        default:
            break
        }
    }
    
}
