# launchpad-controller

MacOS application to read and write a novation launchpad.

I wrote this mostly just to see if I could, so take it for what it is.

This is VERY MUCH a work in progress, and expect frequent updates.
Nonetheless, if you find it interesting and useful, have at it.

I'll be adding more functionality as time goes by.  I want to add the ability
to set colors, and maybe even a game or two, just to experiment.

There are no packages yet.  You'll have to build through XCode.

contact me at rmiller@duskglow.com for questions or support.
